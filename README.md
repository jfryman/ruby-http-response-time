# ruby-http-response-time

Small demo to calculate response times to GitLab.com from my current location

## Usage

```
bundle install
bundle exec ./ruby-http-response-time.rb -v
```
