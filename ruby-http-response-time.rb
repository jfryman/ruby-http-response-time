#!/usr/bin/env ruby

require 'optparse'
require 'ostruct'
require 'net/http'
require 'descriptive_statistics'
require 'pp'

RUN_TIME   = 5
PAUSE_TIME = 1
TARGET     = "https://gitlab.com"

class HTTP_Poller
  def initialize(options)
    @target     ||= options.target
    @run_time   ||= options.run_time
    @pause_time ||= options.pause_time
    @timings    ||= []
    @start_time ||= ""
    @verbose    ||= options.verbose
  end

  def fetch(uri, limit = 10)
    raise ArgumentError, 'too many HTTP redirects' if limit == 0

    response = Net::HTTP.get_response(URI(uri))

    case response
    when Net::HTTPSuccess then
      response
    when Net::HTTPRedirection then
      location = response['location']
      warn "redirected to #{location}" if @verbose
      fetch(location, limit - 1)
    else
      response.value
    end
  end

  def poll
    warn "Beginning poll for #{@run_time} minutes"
    end_poll_time = Time.now + (@run_time * 60)

    while Time.now < end_poll_time
      # Get the current time
      start_time = Time.now

      # Grab our target
      self.fetch(@target)

      # Calculate how long it took
      response_time = Time.now - start_time

      # Jam it for later processing
      @timings << response_time

      warn "Response to #{@target} took #{response_time} seconds" if @verbose
      sleep @pause_time
    end

    self.display_http_response_time_results
  end

  def display_http_response_time_results
    pp @timings.descriptive_statistics
  end
end

# Main application
## Bit of duplicate code, but allows for overwriting at the command line
options            ||= OpenStruct.new
options.run_time   ||= RUN_TIME
options.pause_time ||= PAUSE_TIME
options.target     ||= TARGET
options.verbose    ||= false

opt_parser = OptionParser.new do |opts|
  opts.banner = "Usage: ruby-http-response-time.rb [options]"

  opts.on("-d", "--duration DURATION", Integer, "Duration of run (in minutes) [default: 5]") do |time|
    options.time = time
  end

  opts.on("-p", "--pause PAUSE", Float, "Duration of pause between requests (in seconds) [default: 1]") do |pause|
    options.pause = pause
  end

  opts.on("-t", "--target TARGET", "HTTP endpoint target to test response time") do |target|
    options.target = target
  end

  opts.on("-v", "--verbose", "Enable verbose logging [default: false]") do
    options.verbose = true
  end
end

opt_parser.parse!(ARGV)
poller = HTTP_Poller.new(options)
poller.poll
